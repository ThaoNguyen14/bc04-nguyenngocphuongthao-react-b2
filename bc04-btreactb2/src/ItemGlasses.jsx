import React, { Component } from "react";

export default class ItemGlasses extends Component {
  onClickHandler = (url) => {
    this.props.onClickHandler(url);
  };

  render() {
    let url = this.props.detail.url;
    return (
      <img
        className="img"
        src={url}
        alt=""
        onClick={() => this.onClickHandler(this.props.detail)}
      />
    );
  }
}
