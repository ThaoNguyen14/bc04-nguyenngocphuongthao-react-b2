import React, { Component } from "react";
import ItemGlasses from "./ItemGlasses";

export default class listGlasses extends Component {
  onClickHandler = (detail) => {
    this.props.onClickHandler(detail);
  };
  render() {
    return (
      <div className="container row">
        {this.props.data.map((item, index) => {
          return (
            <div className=" col-2 pt-5 " key={index}>
              <ItemGlasses detail={item} onClickHandler={this.onClickHandler} />
            </div>
          );
        })}
      </div>
    );
  }
}
