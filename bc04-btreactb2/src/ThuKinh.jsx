import React, { Component } from "react";
import ListGlasses from "./listGlasses";
import { dataGlasses } from "./dataGlasses";

export default class ThuKinh extends Component {
  state = {
    glassesArr: dataGlasses,
    modelGlasses: "",
    name: "",
    desc: "",
    display: "none",
  };
  onClickHandler = (detail = {}) => {
    const { url, name, desc } = detail;
    this.setState({ modelGlasses: url, name, desc, display: "block" });
  };
  render() {
    return (
      <fragment>
        <div className=" title p-4 text-center font-weight-bold text-light ">
          TRY GLASSES APP ONLINE
        </div>
        <div className="model mt-5">
          <img className="img-fluid" src="./glassesImage/model.jpg" alt="" />
          <img className="model-glasses" src={this.state.modelGlasses} alt="" />
          <div className="model-desc" style={{ display: this.state.display }}>
            <div className="name">{this.state.name}</div>
            <div className="desc">{this.state.desc}</div>
          </div>
        </div>
        <div className="glasses m-5 mb-0">
          <ListGlasses
            data={this.state.glassesArr}
            onClickHandler={this.onClickHandler}
          />
        </div>
      </fragment>
    );
  }
}
